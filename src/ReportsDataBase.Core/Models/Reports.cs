﻿namespace ReportsDataBase.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Reports : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [MaxLength(80)]
        public string Nombre { get; set; } // ](80) NOT NULL,
        [MaxLength(120)]
        public string Descripcion { get; set; } // ](120) NULL,
        [MaxLength(50)]
        public string Vista { get; set; } // ](50) NULL,
        [MaxLength(50)]
        public string Archivo { get; set; } // ](50) NULL,
        [MaxLength(120)]
        public string Url { get; set; } // ](120) NULL,
        [MaxLength(50)]
        public string ResourceFile { get; set; } // ](50) NULL,
        [MaxLength(100)]
        public string Tabla { get; set; } // ](100) NULL,
        [Required]
        public int ReportsTypesId { get; set; } // ] NOT NULL,

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string Consulta { get; private set; }

        public int? appID { get; set; } // ] NULL,
        [MaxLength(5000)]
        public string CustomConsulta { get; set; } // ](5000) NULL,
        [MaxLength(2000)]
        public string textoDetalleDescripcion { get; set; } // ](2000) NULL,

        [ForeignKey("ReportsTypesId")]
        public virtual ReportsTypes ReportsTypes { get; set; }

        public virtual ICollection<ReportsFilters> ReportsFilters { get; set; }
    }
}
