﻿
namespace ReportsDataBase.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ReportsTypes : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        [MaxLength(50)]
        public string Nombre { get; set; } // ](50) NOT NULL,
        [MaxLength(120)]
        public string Descripcion { get; set; } // ](120) NULL,

        public virtual ICollection<Reports> Reports { get; set; }
    }
}
