﻿namespace ReportsDataBase.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ReportsFilters : GD.GdEntityWithoutTenant<int>
    {
        [Required]
        public int ReportsId { get; set; } // ] NOT NULL,
        [Required]
        [MaxLength(10)]
        public string Tipo { get; set; } // ](10) NOT NULL,
        [Required]
        [MaxLength(50)]
        public string NombreCampo { get; set; } // ](50) NOT NULL,
        [Required]
        [MaxLength(50)]
        public string NombreVariable { get; set; } // ](50) NOT NULL,
        [MaxLength(50)]
        public string NombreVariableHasta { get; set; } // ](50) NULL,
        [MaxLength(1000)]
        public string consulta { get; set; } // ](1000) NULL,
        [MaxLength(50)]
        public string NombreVisual { get; set; } // ](50) NULL,
        public bool tieneConsultas { get; set; } // ] NULL,
        public int? Orden { get; set; } // ] NULL,
        [MaxLength(50)]
        public string Placeholder { get; set; }
        [MaxLength(50)]
        public string PlaceholderHasta { get; set; }
        [ForeignKey("ReportsId")]
        public virtual Reports Reports { get; set; }

        public virtual ICollection<ReportsQueries> ReportsQuerys { get; set; }

    }
}
