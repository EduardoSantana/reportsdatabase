﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsDataBase.GD
{
    public class GdPagedResultRequestDto : LimitedResultRequestDto, IPagedResultRequest, ILimitedResultRequest
    {
        public string TextFilter { get; set; }

        [Range(0, int.MaxValue)]
        public virtual int SkipCount { get; set; }
    }

    public class GdPagedResultUserRequestDto : GdPagedResultRequestDto
    {
        public int? id { get; set; }
    }

}
