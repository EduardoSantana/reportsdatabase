﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsDataBase.GD
{
    public class GdEntityWithTenant<TLlavePrimaria> : Entity<TLlavePrimaria>, IEntity<TLlavePrimaria>, IPassivable, IMustHaveTenant, IFullAudited
    {
        public int TenantId { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }

        public long? CreatorUserId { get; set; }

        public DateTime CreationTime { get; set; }

        public long? LastModifierUserId { get; set; }

        public DateTime? LastModificationTime { get; set; }

        public long? DeleterUserId { get; set; }

        public DateTime? DeletionTime { get; set; }
    }

}
