﻿using Abp.Authorization;
using ReportsDataBase.Authorization.Roles;
using ReportsDataBase.Authorization.Users;

namespace ReportsDataBase.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
