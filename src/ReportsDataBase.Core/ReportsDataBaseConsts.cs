﻿namespace ReportsDataBase
{
    public class ReportsDataBaseConsts
    {
        public const string LocalizationSourceName = "ReportsDataBase";

        public const bool MultiTenancyEnabled = true;
    }
}