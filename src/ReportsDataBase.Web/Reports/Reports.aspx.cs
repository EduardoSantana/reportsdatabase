﻿using Microsoft.Reporting.WebForms;
using ReportsDataBase.EntityFramework;
using ReportsDataBase.Web.Reports.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReportsDataBase.Web.Reports
{
    public partial class Reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form.Count > 0 || Request.QueryString.Count > 0)
            {
                var r = new AutomaticReports(3, Request);
                var d = r.ImprimirReporteAutomatico();
                EjecuarReporte(d);
            }
            else
            {
                ReportViewer1.Visible = false;
                divMensaje.Visible = true;
            }
        }
        public void EjecuarReporte(ReportsResults data)
        {
            var dataSetName = "DataSet1";
            
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath(data.ReportsUrlName);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(dataSetName, data.Data));

            if (!data.ReportsType.Contains("web"))
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                byte[] bytes = ReportViewer1.LocalReport.Render(
                    data.ReportsType,
                    null,
                    out mimeType,
                    out encoding,
                    out filenameExtension,
                    out streamids,
                    out warnings);

                using (FileStream fs = new FileStream(Server.MapPath(data.ReportsOutPutFileName + "." + filenameExtension), FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }

                string pdfPath = MapPath(data.ReportsOutPutFileName + "." + filenameExtension);
                Response.ContentType = mimeType;
                Response.AppendHeader("content-disposition", "attachment; filename=" + pdfPath);
                Response.TransmitFile(pdfPath);
                Response.End();
            }
        }
    }
}