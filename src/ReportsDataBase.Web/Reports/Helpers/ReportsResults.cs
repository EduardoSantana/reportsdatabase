﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ReportsDataBase.Web.Reports.Helpers
{
    public class ReportsResults
    {
        public ReportsResults()
        {
            this.Data = new DataTable();
        }
        public string ReportsType { get; set; }
        public string ReportsUrlName { get; set; }
        public string ReportsOutPutFileName { get; set; }
        public DataTable Data { get; set; }
    }
}