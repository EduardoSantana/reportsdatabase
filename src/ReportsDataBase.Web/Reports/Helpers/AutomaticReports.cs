﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using System.Reflection;
using System.Drawing;
using System.Text;
using Abp.Extensions;
using ReportsDataBase.EntityFramework;
using System.Web;
using System.Collections.Generic;

namespace ReportsDataBase.Web.Reports.Helpers
{
    public class AutomaticReports
    {

        #region "Declaracion de Variables"

        private HttpRequest Request { get; set; }
        private string strRutaEjecucion
        { get { return AppDomain.CurrentDomain.BaseDirectory; } }
        private int RerportId { get; set; }
        private string ReportTypeId { get; set; }
        private int TipoVariable { get; set; }
        private List<string> AcceptedFormats { get { return new List<string> { "excel", "web", "word", "pdf" }; } }

        #endregion

        #region "Metodos de Pagina"

        public AutomaticReports(int _tipoVariable, HttpRequest _request)
        {
            Request = _request;
            RerportId = Convert.ToInt32(getValorVariable("", -1, _tipoVariable));
            ReportTypeId = getValorVariable("", -2, _tipoVariable);
            TipoVariable = _tipoVariable;
        }

        #endregion

        #region "Utilidades"

        public string GetNombreArchivo(string strName)
        {
            string retVal = "";

            if (File.Exists(strRutaEjecucion + "\\" + strName))
            {
                try
                {
                    File.Delete(strRutaEjecucion + "\\" + strName);

                }
                catch (Exception ex)
                {
                }

                var arreglo = strName.Split("-");
                var arreglo2 = arreglo.GetValue(1).ToString().Split(".");
                Random prng = new Random();
                retVal = arreglo.GetValue(0).ToString() + "-" + Convert.ToInt32(prng.Next(1, 9)).ToString() + "." + arreglo2.GetValue(1).ToString();

            }
            else
            {
                retVal = strName;
            }

            return retVal;
        }

        private void Desactivar(string message = "")
        {
            //reporteVisor.Visible = False
            //crsOrigenReporte.Visible = False
            //pnlError.Visible = True
            //lbError.Text = message
        }

        public static byte[] Image_to_Byte(Image Imagen)
        {

            byte[] boofImagen = null;
            try
            {
                FileStream fs = new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate, FileAccess.ReadWrite);
                Imagen.Save(fs, System.Drawing.Imaging.ImageFormat.Png);
                fs.Position = 0;
                int imlengt = Convert.ToInt32(fs.Length);
                boofImagen = new byte[imlengt + 1];
                fs.Read(boofImagen, 0, imlengt);
                fs.Close();
                return boofImagen;

            }
            catch (Exception ex)
            {
                boofImagen = new byte[1];
                return boofImagen;
            }
        }

        //Método utilizado para hacer consultas generales con ADO
        public static DataTable MetGeneral(string Consulta)
        {
            DataTable Tabla = new DataTable();

            try
            {
                var db = new ReportsDataBaseDbContext();
                SqlConnection Con = new SqlConnection(db.Database.Connection.ConnectionString);
                SqlDataAdapter Adapter = new SqlDataAdapter(Consulta, Con);
                Adapter.Fill(Tabla);

            }
            catch (Exception ex)
            {
            }

            return Tabla;
        }


        #endregion

        #region "Proceso de Filtros Automaticos"

        public ReportsResults ImprimirReporteAutomatico()
        {
            return ImprimirReporteAutomatico(RerportId, ReportTypeId, TipoVariable);
        }

        /// <summary>
        /// Imprime reporte desde un ID
        /// </summary>
        /// <param name="intReporteID"></param>
        /// <param name="strTipoReporte"></param>
        /// <remarks></remarks>
        private ReportsResults ImprimirReporteAutomatico(int intReporteID, string strTipoReporte, int intTipoVariable)
        {

            if (intReporteID > 0)
            {
                ReportsDataBaseDbContext db = new ReportsDataBaseDbContext();

                var objReporte = db.Reports.Find(intReporteID);

                string Consulta = GetConsultaReporte(objReporte, intTipoVariable);

                var data = MetGeneral(Consulta);

                string strRutaArchivoBuscar = objReporte.Url + objReporte.Archivo;

                //if (File.Exists(strRutaArchivoBuscar))
                //{
                //    Random prng = new Random();
                //    var arreglo = strRutaArchivoBuscar.Split(".");
                //    var strRutaArchivoBuscarNew = strRutaArchivoBuscar.Replace(arreglo.GetValue(0).ToString(), arreglo.GetValue(0).ToString() + prng.Next(10, 99).ToString());
                //    File.Copy(strRutaArchivoBuscar, strRutaArchivoBuscarNew, true);
                //    strRutaArchivoBuscar = strRutaArchivoBuscarNew;
                //}

                try
                {
                    if (data.Rows.Count > 0)
                    {
                        //Entra en Try por si acaso no existe la foto.
                        try
                        {

                            //Se crea la columna de tipo Byte() para guradar la imagen ya convertida.
                            //datos.Columns.Add(New DataColumn("Logo", GetType(Byte())))
                            //Se convierte la imagen desde su ubicacion fisica
                            //Dim logo() As Byte = Image_to_Byte(New Bitmap(Request.PhysicalApplicationPath + "\" + "Images\Logo.png"))
                            //data.Rows(0)("Logo") = logo

                        }
                        catch (Exception ex)
                        {
                        }

                        //Busca la clase de recurso.
                        //Dim ClaseRecurso As String = objReporte.ResourceFile
                        //Bucle de asignacion de valores del recurso.
                        //If ClaseRecurso IsNot Nothing AndAlso ClaseRecurso.Trim <> "" AndAlso ClaseRecurso.Length > 0 Then
                        //	For Each PField As ParameterField In crsOrigenReporte.ReportDocument.ParameterFields
                        //		'Dim valorRecurso As String = CType(GetGlobalResourceObject(ClaseRecurso, PField.Name), String)
                        //		'crsOrigenReporte.ReportDocument.SetParameterValue(PField.Name, valorRecurso)
                        //	Next
                        //End If

                        //Mostrar el reporte.
                        //this.MostrarReporte(strTipoReporte, objReporte.Nombre);

                        return new ReportsResults()
                        {
                            Data = data,
                            ReportsType = strTipoReporte,
                            ReportsOutPutFileName = objReporte.Nombre,
                            ReportsUrlName = strRutaArchivoBuscar
                        };


                    }
                    else
                    {
                        throw new ArgumentException("ERROR AL CARGAR LOS DATOS");
                    }

                }
                catch (Exception ex)
                {

                    string strMensajeMostrar = ex.Message + "<br>" + strRutaArchivoBuscar;
                    //if (global::cpPrintingModuleV2.My.MySettings.Default("VerStackTrace").ToString.Trim == "1")
                    //{
                    //    strMensajeMostrar += "<br>" + ex.StackTrace;
                    //}

                    Desactivar(strMensajeMostrar);
                }

            }
            else
            {
                Desactivar();
            }
            return new ReportsResults();
        }

        /// <summary>
        /// Concatena la consulta y los filtros.
        /// </summary>
        /// <param name="objReporte">instacia de st_reportes</param>
        /// <param name="tipo">entero para designar que busca: variable o control.</param>
        /// <returns>retorna la cadena de string que es la consulta mas el filtro.</returns>
        /// <remarks>solo simplifica la concatenacion.</remarks>
        public string GetConsultaReporte(ReportsDataBase.Models.Reports objReporte, int tipo)
        {
            string _retVal = "";
            string _Consulta = objReporte.Consulta;
            dynamic _Condicion = getFiltros(objReporte, tipo);
            _retVal = _Consulta + _Condicion;
            return _retVal;
        }

        /// <summary>
        /// Devuelve un Where o un And Dependiendo de que tipo sea el filtro.
        /// </summary>
        /// <param name="intCantFiltros">Si es el 1ro o que numero de filtro es.</param>
        /// <returns>Devuelve un String.</returns>
        /// <remarks>Recuerda que el filtro uno sera where.</remarks>
        public string getFiltroAppend(int intCantFiltros)
        {
            string retVal = "";

            if (intCantFiltros == 1)
            {
                retVal = " WHERE ";
            }
            else if (intCantFiltros == -1)
            {
                retVal = " ORDER BY ";
            }
            else
            {
                retVal = " AND ";
            }

            return retVal;
        }

        /// <summary>
        /// Esta Funcion arma una cadena de los filtros necesarios tomando los datos de la base de datos.
        /// </summary>
        /// <param name="obj">Objeto o Instancia de un reporte en la tabla rep_Reportes.</param>
        /// <param name="tipoBuscar">Tipo de que tipo de variable sale el valor. Variables = 0 , Control = 1.</param>
        /// <returns>Devuelve un String</returns>
        /// <remarks>Favor recordar llenar los valores en la base de datos.</remarks>
        public string getFiltros(ReportsDataBase.Models.Reports obj, int tipoBuscar = 1)
        {
            StringBuilder strFiltros = new StringBuilder();
            int cantFiltros = 0;

            foreach (ReportsDataBase.Models.ReportsFilters item in obj.ReportsFilters)
            {
                string valor = null;
                switch (item.Tipo)
                {
                    case "int":
                        valor = getValorVariable(item.NombreVariable, tipoBuscar);
                        int valorInt = 0;

                        if (valor != null && int.TryParse(valor.ToString(), out valorInt))
                        {
                            cantFiltros += 1;
                            strFiltros.Append(getFiltroAppend(cantFiltros));
                            strFiltros.Append(" ( " + item.NombreCampo + " = " + valorInt.ToString() + " ) ");
                        }

                        break;
                    case "varchar":
                        valor = getValorVariable(item.NombreVariable, tipoBuscar);

                        if (valor != null && !string.IsNullOrEmpty(valor))
                        {
                            cantFiltros += 1;
                            strFiltros.Append(getFiltroAppend(cantFiltros));
                            strFiltros.Append(" ( " + item.NombreCampo + " LIKE '%" + valor + "%' ) ");
                        }

                        break;
                    case "datetime":
                        string valorDesde = getValorVariable(item.NombreVariable, tipoBuscar);
                        string valorHasta = getValorVariable(item.NombreVariableHasta, tipoBuscar);
                        bool valFechaDesde = false;
                        bool valFechaHasta = false;
                        DateTime valorPonerFiltro3a;
                        DateTime valorPonerFiltro3b;

                        valFechaDesde = DateTime.TryParseExact(valorDesde, "yyyy/MM/dd", CultureInfo.CurrentCulture, DateTimeStyles.None, out valorPonerFiltro3a);
                        valFechaHasta = DateTime.TryParseExact(valorHasta, "yyyy/MM/dd", CultureInfo.CurrentCulture, DateTimeStyles.None, out valorPonerFiltro3b);

                        if (valFechaDesde && valFechaHasta)
                        {
                            cantFiltros += 1;
                            strFiltros.Append(getFiltroAppend(cantFiltros));
                            strFiltros.Append(" ( " + item.NombreCampo + " BETWEEN '" + valorPonerFiltro3a.ToString("yyyy-MM-dd 00:00:00.000") + "' AND '" + valorPonerFiltro3b.ToString("yyyy-MM-dd 23:59:59.999") + "' ) ");
                        }
                        break;
                }
            }
            foreach (ReportsDataBase.Models.ReportsFilters item in obj.ReportsFilters)
            {
                switch (item.Tipo)
                {
                    case "orderby":
                        strFiltros.Append(getFiltroAppend(-1));
                        strFiltros.Append(" " + item.NombreCampo + " ");
                        break;
                }
            }
            return strFiltros.ToString();
        }


        /// <summary>
        /// Obtiene el valor de una variable.
        /// </summary>
        /// <param name="variable">Nombre de una variable</param>
        /// <param name="intTipo">Para especificar el tipo de variable a rescatar o buscar. 
        /// Si es 0 buscar una varible Global,
        /// Si es 1 buscar un nombre de control, 
        /// Si es 2 busca un Query String, 
        /// Si es 3 busca en un Post
        /// </param>
        /// <returns>Objeto</returns>
        /// <remarks>Recuerda que la variable debe de ser declarada publica para ser encontrada.</remarks>
        public string getValorVariable(string variable, int intTipo, int intTipoNegativo = 0)
        {
            // string strSeparador = "=";
            string retVal = null;
            int intVal = 0;

            switch (intTipo)
            {
                case -2:
                    // Para que me devuelva el 2do valor del arreglo.

                    switch (intTipoNegativo)
                    {

                        case 2:
                            // Proviene de Query String
                            foreach (string req in Request.QueryString)
                            {
                                intVal += 1;
                                string _NombreControl = req.ToString();
                                string _ValorControl = Request.QueryString[_NombreControl].ToString();
                                if (intVal == 2)
                                {
                                    //if ((_ValorControl == "excel" || _ValorControl == "excelrecord" || _ValorControl == "noformat" || _ValorControl == "crystal"))
                                    if (AcceptedFormats.Contains(_ValorControl))
                                    {
                                        retVal = _ValorControl;
                                    }
                                    else
                                    {
                                        retVal = "pdf";
                                    }
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }

                            break;
                        case 3:
                            // Proviene de un Post
                            foreach (string req in Request.Form)
                            {
                                intVal += 1;
                                string _NombreControl = req.ToString();
                                string _ValorControl = Request.Form[_NombreControl].ToString();
                                if (intVal == 2)
                                {
                                    //if ((_ValorControl == "excel" || _ValorControl == "excelrecord" || _ValorControl == "noformat" || _ValorControl == "crystal"))
                                    if(AcceptedFormats.Contains(_ValorControl))
                                    {
                                        retVal = _ValorControl;
                                    }
                                    else
                                    {
                                        retVal = "pdf";
                                    }
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }

                            break;
                        case 4:

                            //foreach (void req_loopVariable in Module1.LinesArgsFromFileString)
                            //{
                            //    req = req_loopVariable;
                            //    intVal += 1;
                            //    if (intVal == 2)
                            //    {
                            //        dynamic arreglo = req.ToString.Split(strSeparador);
                            //        string _ValorControl = arreglo.GetValue(valoresArreglo.valorControl);
                            //        if ((_ValorControl == "excel" || _ValorControl == "excelrecord" || _ValorControl == "noformat" || _ValorControl == "printer" || _ValorControl == "crystal"))
                            //        {
                            //            retVal = _ValorControl;
                            //        }
                            //        else
                            //        {
                            //            retVal = "pdf";
                            //        }
                            //        break; // TODO: might not be correct. Was : Exit For
                            //    }
                            //}

                            break;
                    }

                    break;
                case -1:
                    // Para que me de el primer valor del arreglo.

                    switch (intTipoNegativo)
                    {

                        case 2:
                            // QUERY STRING
                            foreach (string req in Request.QueryString)
                            {
                                string _NombreControl = req.ToString();
                                string _ValorControl = Request.QueryString[_NombreControl];
                                retVal = _ValorControl;
                                break; // TODO: might not be correct. Was : Exit For
                            }

                            break;
                        case 3:
                            //POST
                            foreach (string req in Request.Form)
                            {
                                string _NombreControl = req.ToString();
                                string _ValorControl = Request.Form[_NombreControl];
                                retVal = _ValorControl;
                                break; // TODO: might not be correct. Was : Exit For
                            }

                            break;

                        case 4:

                            //foreach (string req in Module1.LinesArgsFromFileString)
                            //{
                            //    intVal += 1;
                            //    if (intVal == 1)
                            //    {
                            //        var arreglo = req.ToString().Split(strSeparador);
                            //        string _ValorControl = arreglo[valoresArreglo.valorControl];
                            //        retVal = _ValorControl;
                            //        break; // TODO: might not be correct. Was : Exit For
                            //    }
                            //}

                            break;
                    }

                    break;

                case 0:
                    // desde las variables declaradas en la misma pagina

                    FieldInfo[] fields = this.GetType().GetFields();

                    foreach (FieldInfo fld in fields)
                    {
                        string name = fld.Name;
                        dynamic value = fld.GetValue(this);
                        Type typ = fld.FieldType;
                        if (name == variable)
                        {
                            retVal = value;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }

                    break;

                case 1:

                    break;
                // Controles de pagina

                //Dim c As Object = FindControl(variable)
                //If TypeOf c Is TextBox Then
                //	retVal = DirectCast(c, TextBox).Text
                //ElseIf TypeOf c Is CheckBox Then
                //	retVal = DirectCast(c, CheckBox).Checked
                //ElseIf TypeOf c Is DropDownList Then
                //	retVal = DirectCast(c, DropDownList).SelectedValue
                //End If


                case 2:
                    // Query String

                    foreach (string req in Request.QueryString)
                    {
                        var arreglo = req.ToString().Split("$");
                        var NumeroMaximo = arreglo.Length;
                        if (arreglo[NumeroMaximo - 1] == variable)
                        {
                            string _NombreControl = req.ToString();
                            string _ValorControl = Request.QueryString[_NombreControl];
                            retVal = _ValorControl;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }

                    break;
                case 3:
                    // Post String

                    foreach (string req in Request.Form)
                    {
                        var arreglo = req.ToString().Split("$");
                        var NumeroMaximo = arreglo.Length;
                        if (arreglo[NumeroMaximo - 1] == variable)
                        {
                            string _NombreControl = req.ToString();
                            var _ValorControl = Request.Form[_NombreControl];
                            retVal = _ValorControl;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }

                    break;

                case 4:

                    //foreach (void req_loopVariable in Module1.LinesArgsFromFileString)
                    //{
                    //    req = req_loopVariable;
                    //    dynamic arreglo = req.ToString.Split(strSeparador);
                    //    if (arreglo.GetValue(valoresArreglo.variable) == variable)
                    //    {
                    //        object _ValorControl = arreglo.GetValue(valoresArreglo.valorControl);
                    //        retVal = _ValorControl;
                    //        break; // TODO: might not be correct. Was : Exit For
                    //    }
                    //}

                    break;
            }

            return retVal;

        }

        #endregion

    }
}
