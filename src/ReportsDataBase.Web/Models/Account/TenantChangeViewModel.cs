using Abp.AutoMapper;
using ReportsDataBase.Sessions.Dto;

namespace ReportsDataBase.Web.Models.Account
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}