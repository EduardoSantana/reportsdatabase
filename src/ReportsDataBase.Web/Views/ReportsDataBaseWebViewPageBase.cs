﻿using Abp.Web.Mvc.Views;

namespace ReportsDataBase.Web.Views
{
    public abstract class ReportsDataBaseWebViewPageBase : ReportsDataBaseWebViewPageBase<dynamic>
    {

    }

    public abstract class ReportsDataBaseWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected ReportsDataBaseWebViewPageBase()
        {
            LocalizationSourceName = ReportsDataBaseConsts.LocalizationSourceName;
        }
    }
}