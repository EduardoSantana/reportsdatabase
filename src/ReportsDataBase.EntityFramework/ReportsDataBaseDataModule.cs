﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using ReportsDataBase.EntityFramework;

namespace ReportsDataBase
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(ReportsDataBaseCoreModule))]
    public class ReportsDataBaseDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ReportsDataBaseDbContext>());

            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
