namespace ReportsDataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tester6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportsFilters", "Placeholder", c => c.String(maxLength: 50));
            AddColumn("dbo.ReportsFilters", "PlaceholderHasta", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReportsFilters", "PlaceholderHasta");
            DropColumn("dbo.ReportsFilters", "Placeholder");
        }
    }
}
