﻿using ReportsDataBase.EntityFramework;
using EntityFramework.DynamicFilters;

namespace ReportsDataBase.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly ReportsDataBaseDbContext _context;

        public InitialHostDbBuilder(ReportsDataBaseDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
