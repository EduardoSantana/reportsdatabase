namespace ReportsDataBase.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class tester3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReportsQueries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ValueId = c.String(nullable: false, maxLength: 20),
                        ValueDisplay = c.String(nullable: false, maxLength: 40),
                        ReportsFiltersId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsQueries_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReportsFilters", t => t.ReportsFiltersId, cascadeDelete: true)
                .Index(t => t.ReportsFiltersId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReportsQueries", "ReportsFiltersId", "dbo.ReportsFilters");
            DropIndex("dbo.ReportsQueries", new[] { "ReportsFiltersId" });
            DropTable("dbo.ReportsQueries",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsQueries_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
