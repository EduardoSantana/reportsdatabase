namespace ReportsDataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tester31 : DbMigration
    {
        public override void Up()
        {
            Sql("alter table [dbo].[Reports] add [Consulta]  AS ([dbo].[fn_Campos]([Tabla]))");
        }
        
        public override void Down()
        {
            Sql("alter table [dbo].[Reports] drop column Consulta");
        }
    }
}
