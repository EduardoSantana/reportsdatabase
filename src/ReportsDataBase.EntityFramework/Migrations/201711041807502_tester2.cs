namespace ReportsDataBase.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class tester2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 80),
                        Descripcion = c.String(maxLength: 120),
                        Vista = c.String(maxLength: 50),
                        Archivo = c.String(maxLength: 50),
                        Url = c.String(maxLength: 120),
                        ResourceFile = c.String(maxLength: 50),
                        Tabla = c.String(maxLength: 100),
                        ReportsTypesId = c.Int(nullable: false),
                        appID = c.Int(),
                        CustomConsulta = c.String(),
                        textoDetalleDescripcion = c.String(maxLength: 2000),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Reports_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReportsTypes", t => t.ReportsTypesId, cascadeDelete: true)
                .Index(t => t.ReportsTypesId);
            
            CreateTable(
                "dbo.ReportsFilters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReportsId = c.Int(nullable: false),
                        Tipo = c.String(nullable: false, maxLength: 10),
                        NombreCampo = c.String(nullable: false, maxLength: 50),
                        NombreVariable = c.String(nullable: false, maxLength: 50),
                        NombreVariableHasta = c.String(maxLength: 50),
                        consulta = c.String(maxLength: 1000),
                        NombreVisual = c.String(maxLength: 50),
                        tieneConsultas = c.Boolean(nullable: false),
                        Orden = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsFilters_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Reports", t => t.ReportsId, cascadeDelete: true)
                .Index(t => t.ReportsId);
            
            CreateTable(
                "dbo.ReportsTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Descripcion = c.String(maxLength: 120),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatorUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reports", "ReportsTypesId", "dbo.ReportsTypes");
            DropForeignKey("dbo.ReportsFilters", "ReportsId", "dbo.Reports");
            DropIndex("dbo.ReportsFilters", new[] { "ReportsId" });
            DropIndex("dbo.Reports", new[] { "ReportsTypesId" });
            DropTable("dbo.ReportsTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ReportsFilters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReportsFilters_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Reports",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Reports_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
