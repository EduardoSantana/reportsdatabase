﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace ReportsDataBase.EntityFramework.Repositories
{
    public abstract class ReportsDataBaseRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<ReportsDataBaseDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected ReportsDataBaseRepositoryBase(IDbContextProvider<ReportsDataBaseDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class ReportsDataBaseRepositoryBase<TEntity> : ReportsDataBaseRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected ReportsDataBaseRepositoryBase(IDbContextProvider<ReportsDataBaseDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
