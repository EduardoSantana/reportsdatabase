﻿using System.Data.Common;
using Abp.Zero.EntityFramework;
using ReportsDataBase.Authorization.Roles;
using ReportsDataBase.Authorization.Users;
using ReportsDataBase.MultiTenancy;
using ReportsDataBase.Models;
using System.Data.Entity;

namespace ReportsDataBase.EntityFramework
{
    public class ReportsDataBaseDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        //TODO: Define an IDbSet for your Entities...
        public virtual DbSet<Reports> Reports { get; set; }
        public virtual DbSet<ReportsFilters> ReportsFilters { get; set; }
        public virtual DbSet<ReportsTypes> ReportsTypes { get; set; }
        public virtual DbSet<ReportsQueries> ReportsQueries { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public ReportsDataBaseDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in ReportsDataBaseDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of ReportsDataBaseDbContext since ABP automatically handles it.
         */
        public ReportsDataBaseDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public ReportsDataBaseDbContext(DbConnection existingConnection)
         : base(existingConnection, false)
        {

        }

        public ReportsDataBaseDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }
    }
}
