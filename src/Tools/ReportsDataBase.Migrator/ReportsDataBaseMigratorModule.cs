using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using ReportsDataBase.EntityFramework;

namespace ReportsDataBase.Migrator
{
    [DependsOn(typeof(ReportsDataBaseDataModule))]
    public class ReportsDataBaseMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<ReportsDataBaseDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}