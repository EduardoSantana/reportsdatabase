﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ReportsDataBase.Configuration.Dto;

namespace ReportsDataBase.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ReportsDataBaseAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
