﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ReportsDataBase.Configuration.Dto;

namespace ReportsDataBase.Configuration
{
    public interface IConfigurationAppService: IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}