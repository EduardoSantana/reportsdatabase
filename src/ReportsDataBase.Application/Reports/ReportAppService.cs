//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using ReportsDataBase.Authorization; 
using Microsoft.AspNet.Identity; 
using ReportsDataBase.Reports.Dto; 
using System.Collections.Generic; 
using ReportsDataBase.GD;
namespace ReportsDataBase.Reports 
{ 
    [AbpAuthorize(PermissionNames.Pages_Reports)] 
    public class ReportAppService : AsyncCrudAppService<Models.Reports, ReportDto, int, PagedResultRequestDto, CreateReportDto, UpdateReportDto>, IReportAppService 
    { 
        private readonly IRepository<Models.Reports, int> _ReportRepository;
		


        public ReportAppService( 
            IRepository<Models.Reports, int> repository, 
            IRepository<Models.Reports, int> ReportRepository 
            ) 
            : base(repository) 
        { 
            _ReportRepository = ReportRepository; 
			

			
        } 
        public override async Task<ReportDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ReportDto> Create(CreateReportDto input) 
        { 
            CheckCreatePermission(); 
            var Report = ObjectMapper.Map<Models.Reports>(input); 
            await _ReportRepository.InsertAsync(Report); 
            return MapToEntityDto(Report); 
        } 
        public override async Task<ReportDto> Update(UpdateReportDto input) 
        { 
            CheckUpdatePermission(); 
           var Report = await _ReportRepository.GetAsync(input.Id); 
           MapToEntity(input, Report); 
            await _ReportRepository.UpdateAsync(Report); 
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        { 
            var Report = await _ReportRepository.GetAsync(input.Id); 
            await _ReportRepository.DeleteAsync(Report); 
        } 
        protected override Models.Reports MapToEntity(CreateReportDto createInput) 
        { 
            var Report = ObjectMapper.Map<Models.Reports>(createInput); 
            return Report; 
        } 
        protected override void MapToEntity(UpdateReportDto input, Models.Reports Report) 
        { 
            ObjectMapper.Map(input, Report); 
        } 
        protected override IQueryable<Models.Reports> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.Reports> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.Nombre.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.Reports> GetEntityByIdAsync(int id) 
        { 
            var Report = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(Report); 
        } 
        protected override IQueryable<Models.Reports> ApplySorting(IQueryable<Models.Reports> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.Nombre); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ReportDto>> GetAllReports(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ReportDto> ouput = new PagedResultDto<ReportDto>(); 
            IQueryable<Models.Reports> query = query = from x in _ReportRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _ReportRepository.GetAll() 
                        where x.Nombre.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<Reports.Dto.ReportDto>>(query.ToList()); 
            return ouput; 
        } 
		
		public async Task<List<ReportDto>> GetAllReportsForCombo()
        {
            var ReportList = await _ReportRepository.GetAllListAsync(x => x.IsActive == true);

            var Report = ObjectMapper.Map<List<ReportDto>>(ReportList.ToList());

            return Report;
        }
		
    } 
} ;