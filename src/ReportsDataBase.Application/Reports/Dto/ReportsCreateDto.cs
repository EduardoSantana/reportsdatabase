//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace ReportsDataBase.Reports.Dto
{
    [AutoMap(typeof(Models.Reports))]
    public class CreateReportDto : EntityDto<int>
    {
        public int ReportsTypesId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        [Required]
        [MaxLength(80)]
        public string Nombre { get; set; } // ](80) NOT NULL,
        [MaxLength(120)]
        public string Descripcion { get; set; } // ](120) NULL,
        [MaxLength(50)]
        public string Vista { get; set; } // ](50) NULL,
        [MaxLength(50)]
        public string Archivo { get; set; } // ](50) NULL,
        [MaxLength(120)]
        public string Url { get; set; } // ](120) NULL,
        [MaxLength(50)]
        public string ResourceFile { get; set; } // ](50) NULL,
        [MaxLength(100)]
        public string Tabla { get; set; } // ](100) NULL,
        [MaxLength(5000)]
        public string Consulta { get; set; } // ](5000) NULL,
        public int? appID { get; set; } // ] NULL,
        [MaxLength(5000)]
        public string CustomConsulta { get; set; } // ](5000) NULL,
        [MaxLength(2000)]
        public string textoDetalleDescripcion { get; set; } // ](2000) NULL,


    }
}