//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ReportsDataBase.GD;
using ReportsDataBase.Reports.Dto;
using System.Collections.Generic;

namespace ReportsDataBase.Reports
{
      public interface IReportAppService : IAsyncCrudAppService<ReportDto, int, PagedResultRequestDto, CreateReportDto, UpdateReportDto>
      {
            Task<PagedResultDto<ReportDto>> GetAllReports(GdPagedResultRequestDto input);
			Task<List<Dto.ReportDto>> GetAllReportsForCombo();
      }
}