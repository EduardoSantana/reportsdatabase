﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ReportsDataBase.MultiTenancy.Dto;

namespace ReportsDataBase.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
