﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ReportsDataBase.Sessions.Dto;

namespace ReportsDataBase.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
