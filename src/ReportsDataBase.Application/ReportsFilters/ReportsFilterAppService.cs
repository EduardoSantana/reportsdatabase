//Created from Templaste MG

using System.Linq; 
using System.Threading.Tasks; 
using Abp.Application.Services; 
using Abp.Application.Services.Dto; 
using Abp.Authorization; 
using Abp.Domain.Repositories; 
using Abp.IdentityFramework; 
using ReportsDataBase.Authorization; 
using Microsoft.AspNet.Identity; 
using ReportsDataBase.ReportsFilters.Dto; 
using System.Collections.Generic; 
using ReportsDataBase.GD;
namespace ReportsDataBase.ReportsFilters 
{ 
    public class ReportsFilterAppService : AsyncCrudAppService<Models.ReportsFilters, ReportsFilterDto, int, PagedResultRequestDto, CreateReportsFilterDto, UpdateReportsFilterDto>, IReportsFilterAppService 
    { 
        private readonly IRepository<Models.ReportsFilters, int> _reportsFilterRepository;
		

        public ReportsFilterAppService( 
            IRepository<Models.ReportsFilters, int> repository, 
            IRepository<Models.ReportsFilters, int> reportsFilterRepository 
            ) 
            : base(repository) 
        { 
            _reportsFilterRepository = reportsFilterRepository; 
			

			
        } 
        public override async Task<ReportsFilterDto> Get(EntityDto<int> input) 
        { 
            var user = await base.Get(input); 
            return user; 
        } 
        public override async Task<ReportsFilterDto> Create(CreateReportsFilterDto input) 
        { 
            CheckCreatePermission(); 
            var reportsFilter = ObjectMapper.Map<Models.ReportsFilters>(input); 
            await _reportsFilterRepository.InsertAsync(reportsFilter); 
            return MapToEntityDto(reportsFilter); 
        } 
        public override async Task<ReportsFilterDto> Update(UpdateReportsFilterDto input) 
        { 
            CheckUpdatePermission(); 
           var reportsFilter = await _reportsFilterRepository.GetAsync(input.Id); 
           MapToEntity(input, reportsFilter); 
            await _reportsFilterRepository.UpdateAsync(reportsFilter); 
            return await Get(input); 
        } 
        public override async Task Delete(EntityDto<int> input) 
        { 
            var reportsFilter = await _reportsFilterRepository.GetAsync(input.Id); 
            await _reportsFilterRepository.DeleteAsync(reportsFilter); 
        } 
        protected override Models.ReportsFilters MapToEntity(CreateReportsFilterDto createInput) 
        { 
            var reportsFilter = ObjectMapper.Map<Models.ReportsFilters>(createInput); 
            return reportsFilter; 
        } 
        protected override void MapToEntity(UpdateReportsFilterDto input, Models.ReportsFilters reportsFilter) 
        { 
            ObjectMapper.Map(input, reportsFilter); 
        } 
        protected override IQueryable<Models.ReportsFilters> CreateFilteredQuery(PagedResultRequestDto input) 
        { 
            return Repository.GetAll(); 
        } 
        protected  IQueryable<Models.ReportsFilters> CreateFilteredByTextQuery(GD.GdPagedResultRequestDto input) 
        { 
            var resultado =  Repository.GetAll(); 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
                resultado = resultado.Where(x => x.NombreVisual.Contains(input.TextFilter)); 
            return resultado; 
        } 
        protected override async Task<Models.ReportsFilters> GetEntityByIdAsync(int id) 
        { 
            var reportsFilter = Repository.GetAllList().FirstOrDefault(x => x.Id == id); 
            return await Task.FromResult(reportsFilter); 
        } 
        protected override IQueryable<Models.ReportsFilters> ApplySorting(IQueryable<Models.ReportsFilters> query, PagedResultRequestDto input) 
        { 
            return query.OrderBy(r => r.NombreVisual); 
        } 
 
        protected virtual void CheckErrors(IdentityResult identityResult) 
        { 
            identityResult.CheckErrors(LocalizationManager); 
        } 
        
        public async Task<PagedResultDto<ReportsFilterDto>> GetAllReportsFilters(GdPagedResultRequestDto input) 
        { 
            PagedResultDto<ReportsFilterDto> ouput = new PagedResultDto<ReportsFilterDto>(); 
            IQueryable<Models.ReportsFilters> query = query = from x in _reportsFilterRepository.GetAll() 
                                                       select x; 
            if (!string.IsNullOrEmpty(input.TextFilter)) 
            { 
                query = from x in _reportsFilterRepository.GetAll() 
                        where x.NombreVisual.Contains(input.TextFilter) 
                        select x; 
            } 
            ouput.TotalCount = await Task.Run(() => { return query.Count(); }); 
            if (input.SkipCount > 0) 
            { 
                query = query.OrderBy(x => x.Id).Skip(input.SkipCount); 
            }  
            if (input.MaxResultCount > 0) 
            { 
                query = query.Take(input.MaxResultCount); 
            } 
            ouput.Items = ObjectMapper.Map<List<ReportsFilters.Dto.ReportsFilterDto>>(query.ToList()); 
            return ouput; 
        } 
		
		public async Task<List<ReportsFilterDto>> GetAllReportsFiltersForCombo()
        {
            var reportsFilterList = await _reportsFilterRepository.GetAllListAsync(x => x.IsActive == true);

            var reportsFilter = ObjectMapper.Map<List<ReportsFilterDto>>(reportsFilterList.ToList());

            return reportsFilter;
        }
		
    } 
} ;