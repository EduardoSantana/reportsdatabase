//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace ReportsDataBase.ReportsFilters.Dto 
{
        [AutoMap(typeof(Models.ReportsFilters))] 
        public class UpdateReportsFilterDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public int ReportsId {get;set;} 
              public bool tieneConsultas {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}