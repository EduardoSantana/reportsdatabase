//Created from Templaste MG

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.Collections.Generic;
using ReportsDataBase.ReportsQueries.Dto;

namespace ReportsDataBase.ReportsFilters.Dto
{
    [AutoMap(typeof(Models.ReportsFilters))]
    public class ReportsFilterDto : EntityDto<int>
    {
        public int ReportsId { get; set; }
        public bool tieneConsultas { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        [Required]
        [MaxLength(10)]
        public string Tipo { get; set; } // ](10) NOT NULL,
        [Required]
        [MaxLength(50)]
        public string NombreCampo { get; set; } // ](50) NOT NULL,
        [Required]
        [MaxLength(50)]
        public string NombreVariable { get; set; } // ](50) NOT NULL,
        [MaxLength(50)]
        public string NombreVariableHasta { get; set; } // ](50) NULL,
        [MaxLength(1000)]
        public string consulta { get; set; } // ](1000) NULL,
        [MaxLength(50)]
        public string NombreVisual { get; set; } // ](50) NULL,
        public int? Orden { get; set; } // ] NULL,
        [MaxLength(50)]
        public string Placeholder { get; set; }
        [MaxLength(50)]
        public string PlaceholderHasta { get; set; }
        public ICollection<ReportsQueryDto> ReportsQuerys { get; set; }

    }
}