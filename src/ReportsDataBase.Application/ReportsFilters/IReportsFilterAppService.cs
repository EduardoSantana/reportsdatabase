//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ReportsDataBase.GD;
using ReportsDataBase.ReportsFilters.Dto;
using System.Collections.Generic;

namespace ReportsDataBase.ReportsFilters
{
      public interface IReportsFilterAppService : IAsyncCrudAppService<ReportsFilterDto, int, PagedResultRequestDto, CreateReportsFilterDto, UpdateReportsFilterDto>
      {
            Task<PagedResultDto<ReportsFilterDto>> GetAllReportsFilters(GdPagedResultRequestDto input);
			Task<List<Dto.ReportsFilterDto>> GetAllReportsFiltersForCombo();
      }
}