﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ReportsDataBase.Authorization.Accounts.Dto;

namespace ReportsDataBase.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
