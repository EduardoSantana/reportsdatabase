//Created from Templaste MG

using System.Threading.Tasks; 
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ReportsDataBase.GD;
using ReportsDataBase.ReportsQueries.Dto;
using System.Collections.Generic;

namespace ReportsDataBase.ReportsQueries
{
      public interface IReportsQueryAppService : IAsyncCrudAppService<ReportsQueryDto, int, PagedResultRequestDto, CreateReportsQueryDto, UpdateReportsQueryDto>
      {
            Task<PagedResultDto<ReportsQueryDto>> GetAllReportsQueries(GdPagedResultRequestDto input);
			Task<List<Dto.ReportsQueryDto>> GetAllReportsQueriesForCombo();
      }
}