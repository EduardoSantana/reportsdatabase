//Created from Templaste MG

using System; 
using System.ComponentModel.DataAnnotations; 
using Abp.Application.Services.Dto; 
using Abp.Authorization.Users; 
using Abp.AutoMapper; 

namespace ReportsDataBase.ReportsQueries.Dto 
{
        [AutoMap(typeof(Models.ReportsQueries))] 
        public class UpdateReportsQueryDto : EntityDto<int> 
        {
              public int Id {get;set;} 
              public string ValueId {get;set;} 
              public string ValueDisplay {get;set;} 
              public int ReportsFiltersId {get;set;} 
              public bool IsActive {get;set;} 
              public DateTime CreationTime {get;set;} 
              public long? CreatorUserId {get;set;} 

         }
}