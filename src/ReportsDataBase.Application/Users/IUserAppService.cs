using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ReportsDataBase.Roles.Dto;
using ReportsDataBase.Users.Dto;

namespace ReportsDataBase.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
    }
}